package org.crypthing.things.appservice.management.app;

import java.lang.management.ManagementFactory;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.ObjectName;

import org.crypthing.things.appservice.Runner;
import org.crypthing.things.snmp.EncodableString;
import org.crypthing.things.snmp.LifecycleEvent;
import org.crypthing.things.snmp.LifecycleEvent.LifecycleEventType;
import org.crypthing.things.snmp.ProcessingEvent;
import org.crypthing.things.snmp.ProcessingEventListener;
import org.crypthing.things.snmp.SNMPBridge;
import org.json.JSONObject;

public class TrapDispatcher implements ProcessingEventListener, TrapDispatcherMBean
{
	public static final String SERVER_NAME = "TestService";
	public static final String JMX_HOST = System.getProperty("java.rmi.server.hostname");
	public static final String JMX_PORT = System.getProperty("com.sun.management.jmxremote.port");
	public static final int WORKERS = 1;
	public static final String[] ENVIRON = new String[] { "environ" };
	public static final String CONFIG = "config.file";

	private long success;
	private long error;
	private SNMPBridge bridge;
	private long heartbeat;
	private ObjectName serviceName;
	public TrapDispatcher()
	{
		try
		{
			success = 0;
			error = 0;
			heartbeat = 5000;
			serviceName = new ObjectName
			(
				(new StringBuilder(256))
				.append(Runner.MBEAN_PATTERN)
				.append("testEndpoint")
				.toString()
			);
			ManagementFactory.getPlatformMBeanServer().registerMBean(this, serviceName);
			bridge = SNMPBridge.newInstance("org.crypthing.things.snmp.SNMPBridge", "127.0.0.1/8173", "1.51.171");
			final Thread daemon = new Thread(new Heartbeat());
			daemon.setDaemon(true);
			daemon.start();
		}
		catch (final Throwable e) { throw new RuntimeException(e); }
	}
	public void release()
	{
		heartbeat = 0;
		try { ManagementFactory.getPlatformMBeanServer().unregisterMBean(serviceName); }
		catch (final MBeanRegistrationException | InstanceNotFoundException swallowed) {}
	}
	public void success() { ++success; }
	public void failure() { ++error; }
	@Override public void info(ProcessingEvent e) { bridge.notify(e); }
	@Override public void warning(ProcessingEvent e) { bridge.notify(e); }
	@Override public void error(ProcessingEvent e) { bridge.notify(e); }
	private class Heartbeat implements Runnable
	{
		private final JSONObject jmx;
		private Heartbeat()
		{
			jmx = new JSONObject();
			jmx.put("address", JMX_HOST);
			jmx.put("port", JMX_PORT);
		}
		@Override
		public void run()
		{
			while (heartbeat > 0)
			{
				final JSONObject json = new JSONObject();
				json.put("jmx", jmx);
				json.put("success", success);
				json.put("failures", error);
				json.put("workers", WORKERS);
				bridge.notify(new LifecycleEvent(LifecycleEventType.heart, new EncodableString(json.toString())));
				try{ Thread.sleep(heartbeat); } catch(final InterruptedException swallowed) {}
			}
		}
	}
	@Override public void shutdown() {}
	@Override public void shutdown(String key) {}
	@Override public long getSuccessCount() { return success; }
	@Override public long getErrorCount() { return error; }
	@Override public int getWorkerCount() { return 0; }
	@Override
	public String[] getEnvironment() { return ENVIRON; }
	@Override
	public String getConfigFile() { return CONFIG; }
}
