package org.crypthing.things.appservice.management.app;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

import org.crypthing.things.snmp.ErrorBean;
import org.crypthing.things.snmp.ProcessingEvent;
import org.crypthing.things.snmp.SignalBean;
import org.crypthing.things.snmp.ProcessingEvent.ProcessingEventType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestApp
{
	private static final String SERVICE_ARRAY = "[\"" + TrapDispatcher.SERVER_NAME + "\"]";
	private static TrapDispatcher TRAP;

	@BeforeAll
	public static void prepare()
	{
		TRAP = new TrapDispatcher();
	}
	@AfterAll
	public static void release()
	{
		TRAP.release();
	}

	@DisplayName("Test endpoints first response")
	@Test
	@Order(1)
	public void testEndpoints()
	{
		given()
			.when().get("/api/agents")
			.then().statusCode(200).body(is("[]"));
		given()
			.when().get("/api/servers")
			.then().statusCode(200).body(is("[]"));
		given()
			.when().get("/api/rogueservers")
			.then().statusCode(200).body(is(SERVICE_ARRAY));
	}
	@DisplayName("Test rogue promotion")
	@Test
	@Order(2)
	public void testPromotion()
	{
		given()
			.when().get("/api/rogueservers/" + TrapDispatcher.SERVER_NAME + "/promote")
			.then().statusCode(200);
		given()
			.when().get("/api/servers")
			.then().statusCode(200).body(is(SERVICE_ARRAY));
	}
	@DisplayName("Test information structure")
	@Test
	@Order(3)
	public void testService()
	{
		given().when().get("/api/servers/" + TrapDispatcher.SERVER_NAME)
			.then().statusCode(200)
			.body(containsString("\"name\":\"" + TrapDispatcher.SERVER_NAME + "\"")).and()
			.body(containsString("jmx\":{\"address\":\"" + TrapDispatcher.JMX_HOST + "\",\"port\":" + TrapDispatcher.JMX_PORT + "}")).and()
			.body(containsString("lastknowof")).and()
			.body(containsString("startTime")).and()
			.body(containsString("success")).and()
			.body(containsString("failures")).and()
			.body(containsString("workers")).and()
			.body(containsString("heartbeats")).and()
			.body(containsString("env"));
	}
	@DisplayName("Test log info")
	@Test
	@Order(4)
	public void testLogInfo()
	{
		TRAP.info
		(
			new ProcessingEvent
			(
				ProcessingEventType.info,
				new SignalBean
				(
					TrapDispatcher.SERVER_NAME,
					"Log Info"
				).encode()
			)
		);
		given().when().get("/api/servers/" + TrapDispatcher.SERVER_NAME + "/events")
			.then().statusCode(200)
			.body(containsString("\"type\":\"Info\""));
	}
	@DisplayName("Test log warn")
	@Test
	@Order(5)
	public void testLogWarn()
	{
		TRAP.warning
		(
			new ProcessingEvent
			(
				ProcessingEventType.warning,
				new SignalBean
				(
					TrapDispatcher.SERVER_NAME,
					"Log Warning"
				).encode()
			)
		);
		given().when().get("/api/servers/" + TrapDispatcher.SERVER_NAME + "/events")
			.then().statusCode(200)
			.body(containsString("\"type\":\"Warning\""));
	}
	@DisplayName("Test log error")
	@Test
	@Order(6)
	public void testLogError()
	{
		try { throw new RuntimeException("Generated Error"); }
		catch (Throwable e)
		{
			TRAP.error
			(
				new ProcessingEvent
				(
					ProcessingEventType.error,
					new ErrorBean
					(
						TrapDispatcher.SERVER_NAME,
						"Log error",
						e
					).encode()
				)
			);
		}
		given().when().get("/api/servers/" + TrapDispatcher.SERVER_NAME + "/events")
			.then().statusCode(200)
			.body(containsString("\"type\":\"Error\""));
	}
	@DisplayName("Test success and failures")
	@Test
	@Order(7)
	public void testCounters()
	{
		TRAP.success();
		TRAP.failure();
		try { Thread.sleep(5000); }
		catch (InterruptedException e) { throw new RuntimeException(e); }
		given().when().get("/api/servers/" + TrapDispatcher.SERVER_NAME)
			.then().statusCode(200)
			.body(containsString("\"name\":\"" + TrapDispatcher.SERVER_NAME + "\"")).and()
			.body(containsString("jmx\":{\"address\":\"" + TrapDispatcher.JMX_HOST + "\",\"port\":" + TrapDispatcher.JMX_PORT + "}")).and()
			.body(containsString("lastknowof")).and()
			.body(containsString("startTime")).and()
			.body(containsString("\"success\":1")).and()
			.body(containsString("\"failures\":1")).and()
			.body(containsString("workers")).and()
			.body(containsString("heartbeats")).and()
			.body(containsString("env"));
	}
	@DisplayName("Test server demotion")
	@Test
	@Order(8)
	public void testDemotion()
	{
		given()
			.when().get("/api/servers/" + TrapDispatcher.SERVER_NAME + "/demote")
			.then().statusCode(200);
		given()
			.when().get("/api/servers")
			.then().statusCode(200).body(is("[]"));
		given()
			.when().get("/api/rogueservers")
			.then().statusCode(200).body(is(SERVICE_ARRAY));

	}
}
