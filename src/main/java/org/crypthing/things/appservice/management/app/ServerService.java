package org.crypthing.things.appservice.management.app;

import javax.ws.rs.Path;

import org.crypthing.things.appservice.management.rest.ServerRest;

@Path("api/servers")
public class ServerService extends ServerRest {}
