package org.crypthing.things.appservice.management.app;

import javax.ws.rs.Path;

import org.crypthing.things.appservice.management.rest.RogueServers;

@Path("api/rogueservers")
public class RogueService extends RogueServers {}
