package org.crypthing.things.appservice.management.app;

import javax.ws.rs.Path;

import org.crypthing.things.appservice.management.rest.AgentRest;

@Path("api/agents")
public class AgentService extends AgentRest {}
