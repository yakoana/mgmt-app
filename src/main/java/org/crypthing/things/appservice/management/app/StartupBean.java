package org.crypthing.things.appservice.management.app;

import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.crypthing.things.appservice.management.Storage;
import org.crypthing.things.appservice.management.mib.MIB;
import org.crypthing.things.appservice.management.mib.MIB.MIBInitException;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class StartupBean
{
	@ConfigProperty(name = "org.crypthing.thing.storage", defaultValue = "servers.json")
	String storage;
	@ConfigProperty(name = "org.crypthing.things.MIBAddress", defaultValue = "127.0.0.1/8163")
	String mibAddress;
	@ConfigProperty(name = MIB.MAXEVENTS_ENTRY, defaultValue = "128")
	String maxEvents;
	@ConfigProperty(name = MIB.LOG_ENTRY, defaultValue = "true")
	String eventLog;
	private MIB mib;
	void onStart(@Observes StartupEvent evt)
	{
		System.getProperties().setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.crypthing.things.jndi.InitialContextFactory");
		try
		{
			final InitialContext context = new InitialContext();
			context.bind(Storage.RESOURCE, storage);
			context.bind(MIB.MIBADDRESS, mibAddress);
			final Properties props = new Properties();
			props.setProperty(MIB.MAXEVENTS_ENTRY, maxEvents);
			props.setProperty(MIB.LOG_ENTRY, eventLog);
			mib = new MIB();
			mib.init(props);
		}
		catch (final NamingException | MIBInitException e) { throw new RuntimeException(e); }
	}
	void onStop(@Observes ShutdownEvent evt)
	{
		if (mib != null) mib.destroy();
	}
}
